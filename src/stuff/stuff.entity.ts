import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('stuff')
export class Department {
    @PrimaryGeneratedColumn()
    st_id?:number;
    @Column({type:'varchar', length:100})
    st_surname:string;
    @Column({type:'varchar', length:100})
    st_name:string;
    @Column({type:'varchar', length:100})
    st_patronomic:string;
    @Column({type:'date'})
    st_birthday:Date;
    @Column({type:'varchar', length:100})
    st_position:string;
}