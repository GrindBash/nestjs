import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Department } from './department.entity';
import { DepartmentService } from './department.service';


@Crud({
    model: {
        type: Department,
    },
})
@Controller('department')
export class DepartmentController implements CrudController<Department>{
    constructor(public service: DepartmentService) {

    }
}
