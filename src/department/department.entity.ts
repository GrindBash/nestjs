import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('departments')
export class Department {
    @PrimaryGeneratedColumn()
    dep_id?:number;
    @Column({type:'varchar', length:100})
    dep_name:string;
    @Column({type:'varchar', length:100})
    dep_description:string;
}