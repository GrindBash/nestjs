import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from './orm.config';
import { DepartmentModule } from './department/department.module';
import { StuffModule } from './stuff/stuff.module';


@Module({
  imports: [TypeOrmModule.forRoot(config),DepartmentModule, StuffModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
